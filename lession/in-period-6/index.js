var express = require('express');
var app = express();
var fs = require("fs");
var bodyParser = require('body-parser');
var router = express.Router();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static('public'));
app.use(express.static('public1'));


// app.use(function(req, res, next) {
//     console.log('begin middleware');
    
//     req.myname = 'Ha';
//     next();
// })


router.get('/users', function (req, res) {
    console.log(req.myname);
   fs.readFile( __dirname + "/" + "users.json", 'utf8', function (err, data) {
       console.log( data );
       res.end( data );
   });
});

router.put('/users/:id', function (req, res) {
    var id = req.params.id;
    console.log(id);
    fs.readFile( __dirname + "/" + "users.json", 'utf8', function (err, data) {
        console.log( data );
        res.end( id );
    });
 });

router.post('/users', function (req, res) {
    var newUser = req.body;

    fs.readFile(__dirname + "/" + "users.json", 'utf8', function (err, data) {
        var list = JSON.parse(data);
        list.push(newUser);

        fs.writeFile(__dirname + "/" + "users.json", JSON.stringify(list), function (err, data) {
            if(err) {
                res.status(500);
                res.end('error');
            } else {
                res.send(list);
            }
        });
    });
 });
 
 app.use('/', router);

app.listen(8081, function () {
    console.log("Ung dung Node.js dang lang nghe tai dia chi: http://localhost:8081");
})