var path = require("path");

// console.log(__filename);
// console.log(__dirname);

// console.log(path.normalize(__filename))
// console.log(path.join(__dirname, '__filename.js'))
// console.log(path.resolve(__dirname, __filename))
// console.log(path.isAbsolute(__dirname))
// console.log(path.isAbsolute('./server.js'))
// console.log(path.basename(__filename))
// console.log(path.parse(__filename))

console.log('Phuong thuc NORMALIZE : ' +
path.normalize('/test/test1//2slashes/1slash/tab/..'));

console.log('Phuong thuc JOIN : ' + path.join('/test', 'test1', '2slashes/1slash', 'tab',
'..'));

// Resolve mot duong dan tuyet doi
console.log('Phuong thuc RESOLVE : ' + path.resolve('main.js'));