var express = require('express');
var app = express();
var fs = require("fs");
var bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(express.static('public'));

app.get('/users', function (req, res) {
    fs.readFile(__dirname + "/" + "users.json", 'utf8', function (err, data) {
        res.send(JSON.parse(data));
    });
})

app.post('/users', function (req, res) {
    var newUser = req.body;
    fs.readFile(__dirname + "/" + "users.json", 'utf8', function (err, data) {
        var list = JSON.parse(data);
        list.push(newUser);

        fs.writeFile(__dirname + "/" + "users.json", JSON.stringify(list), function (err, data) {
            if(err) {
                res.status(500);
                res.end('error');
            } else {
                res.send(list);
            }
            
        });

    });
})

var server = app.listen(8081, function () {
    var host = server.address().address
    var port = server.address().port
    console.log("Ung dung Node.js dang lang nghe tai dia chi: http://%s:%s", host, port)
})