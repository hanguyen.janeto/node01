var router = require('express').Router();
var fs = require('fs');
var path = require('path');
var auth = require('../middle-ware/auth');
var userController = require('../controller/user.controller');

router.get('/', auth.auth(), getUsers);
router.post('/', createUser);

module.exports = router;

function getUsers(req, res, next) {
    userController.getUsers()
    .then(function(users) {
        res.send(users);
    })
    .catch(function(err) {
        next(err);
    })
}

function createUser(req, res) {
    var newUser = req.body;

    fs.readFile(path.join(__dirname, "../" + "users.json"), 'utf8', function (err, data) {
        var list = JSON.parse(data);
        list.push(newUser);

        fs.writeFile(path.join(__dirname, "../" + "users.json"), JSON.stringify(list), function (err, data) {
            if (err) {
                res.status(500);
                res.end('error');
            } else {
                res.send(list);
            }
        });
    });
}
